package main

import (
    "errors"
    "fmt"
    "io/ioutil"
    "os"
    "strconv"
    "strings"
)

func main() {
    fmt.Println("DEMO")
    run("demo.txt", 5)

    fmt.Println("PROD")
    run("input.txt", 25)
}

func run(file string, preambleSize int) {
    numbers := getNumbers(file)
    vulnerableNumber, err := findVulnerableNumber(numbers, preambleSize)

    if err != nil {
        fmt.Println(err)
    } else {
        fmt.Println("vulnerability is " + strconv.Itoa(vulnerableNumber))
        sequence, err := findSequence(vulnerableNumber, 0, numbers)
        if err != nil {
            fmt.Println(err)
            os.Exit(1)
        }
        min, max := minMax(sequence)
        fmt.Println(min + max)
    }
}

func minMax(numbers []int) (int, int) {
    max := numbers[0]
    min := max

    for _, number := range numbers {
        if number > max {
            max = number
        }
        if number < min {
            min = number
        }
    }
    return max, min
}

func findSequence(targetNumber int, acc int, numbers []int) ([]int, error) {
    //    fmt.Println(strconv.Itoa(targetNumber) + "=" + strconv.Itoa(acc) + "+" + strconv.Itoa(number))

    for i := 0; i < len(numbers); i++ {
        sum := 0
        tmp := i
        ree := make([]int, 0)
        for sum < targetNumber && tmp < len(numbers) {
            number := numbers[tmp]
            if number == targetNumber {
                continue
            }
            sum += number
            ree = append(ree, number)
            tmp++
        }
        if sum == targetNumber {
            return ree, nil
        }
    }
    return nil, errors.New("no sequence found")
}

func findVulnerableNumber(numbers []int, preambleSize int) (int, error) {
    for i := preambleSize; i < len(numbers); i++ {
        if !isNumberSumOfTwo(numbers[i], numbers[i-preambleSize:i]) {
            return numbers[i], nil
        }
    }
    return 0, errors.New("no vulnerability found")
}

func isNumberSumOfTwo(targetNumber int, numbers []int) bool {
    for i, a := range numbers {
        for _, b := range numbers[i:] {
            if a+b == targetNumber {
                return true
            }
        }
    }
    return false
}

func getNumbers(file string) []int {
    input, _ := ioutil.ReadFile(file)
    sanitizedInput := strings.TrimSuffix(string(input), "\n")
    stringNumbers := strings.Split(sanitizedInput, "\n")

    numbers := make([]int, len(stringNumbers))
    for i, stringNumber := range stringNumbers {
        if stringNumber == "" {
            return numbers
        }
        numbers[i], _ = strconv.Atoi(stringNumber)
    }

    return numbers
}
