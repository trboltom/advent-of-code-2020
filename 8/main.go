package main

import (
    "errors"
    "fmt"
    "io/ioutil"
    "regexp"
    "strconv"
    "strings"
)

type Instruction struct {
    name  string
    value int
}

func main() {
    rawInput, _ := ioutil.ReadFile("input.txt")
    lines := strings.Split(string(rawInput), "\n")

    instructions := createInstructions(lines)

    acc, _ := calculateAcc(instructions)
    fmt.Println("Accumulator value is: " + strconv.Itoa(acc))

    acc = fixInfiniteLoop(instructions)
    fmt.Println("After correction, accumulator value is: " + strconv.Itoa(acc))
}

func fixInfiniteLoop(instructions map[int]Instruction) int {
    originalInstructions := make(map[int]Instruction)
    for k, v := range instructions {
        originalInstructions[k] = v
    }

    for i, instruction := range instructions {
        for k, v := range originalInstructions {
            instructions[k] = v
        }

        if instruction.name == "jmp" {
            instruction.name = "nop"
        } else if instruction.name == "nop" {
            instruction.name = "jmp"
        }

        instructions[i] = instruction
        acc, err := calculateAcc(instructions)

        if err == nil {
            return acc
        }
    }

    return -1
}

func calculateAcc(instructions map[int]Instruction) (int, error) {
    acc := 0
    alreadyVisited := make(map[int]bool)

    pointer := 0
    for !alreadyVisited[pointer] {
        alreadyVisited[pointer] = true

        instruction, ok := instructions[pointer]

        if !ok {
            return acc, nil
        }

        if instruction.name == "nop" {
            pointer++
        }
        if instruction.name == "jmp" {
            pointer += instruction.value
        }
        if instruction.name == "acc" {
            pointer++
            acc += instruction.value
        }
    }
    return acc, errors.New("Infinite loop detected")
}

func createInstructions(lines []string) map[int]Instruction {
    instructions := make(map[int]Instruction)
    typeRegex := regexp.MustCompile("[a-z]+")
    valueRegex := regexp.MustCompile("-?\\d+")

    for i, line := range lines {
        if line == "" {
            continue
        }
        insType := typeRegex.FindString(line)
        value, _ := strconv.Atoi(valueRegex.FindString(line))

        instructions[i] = Instruction{name: insType, value: value}
    }

    return instructions
}
