package main

import (
    "fmt"
    "io/ioutil"
    "runtime"
    "sort"
    "strconv"
    "strings"
    "sync"
)

const maintenanceId = "x"

type Bus struct {
    id                  int
    offsetSinceFirstBus int
}

var wg sync.WaitGroup
var found bool

func loadData(fileName string) []*Bus {
    rawInput, _ := ioutil.ReadFile(fileName)
    sanitized := strings.TrimRight(string(rawInput), "\n")
    input := strings.Split(sanitized, "\n")

    busValues := strings.Split(input[1], ",")

    busses := make([]*Bus, 0)

    for i := 0; i < len(busValues); i++ {
        singleBusId := busValues[i]
        if singleBusId == maintenanceId {
            continue
        }
        id, _ := strconv.Atoi(singleBusId)
        busses = append(busses, &Bus{id: id, offsetSinceFirstBus: i})
    }

    return busses
}

func main() {
    run("test5.txt", 0)
    run("demo.txt", 0)
    run("input.txt", 1e14)
}

func run(fileName string, startOffset int) {
    fmt.Println("==============")
    busses := loadData(fileName)
    sort.Slice(busses, func(i, j int) bool {
        return busses[i].id > busses[j].id
    })
    printBusses(busses)
    found = false
    partitionSize := int(5e13)
    partitionIndex := 0
    maxWorkersLimit := runtime.NumCPU()
    workersCnt := 0

    for !found {
        for workersCnt < maxWorkersLimit {
            if found {
                break
            }
            wg.Add(1)
            workersCnt++
            go runPartial(busses, startOffset+partitionIndex*partitionSize, startOffset+(partitionIndex+1)*partitionSize)
            partitionIndex++
        }
        wg.Wait()
        workersCnt = 0
    }
}

func runPartial(busses []*Bus, startOffset int, stopAt int) {
    increment := busses[0].id
    index := startOffset + increment - busses[0].offsetSinceFirstBus - (startOffset % increment)

    for true {
        failed := false
        for i := 0; i < len(busses); i++ {
            bus := busses[i]

            if (index+bus.offsetSinceFirstBus)%bus.id != 0 {
                failed = true
                break
            }
        }
        if !failed {
            found = true
            fmt.Println(">>>>>> result is ", index)
            break
        }
        if index%100000000 == 0 && !found {
            //            fmt.Println(index)
        }
        index += increment
        if index > stopAt {
            // fmt.Println("not found between", startOffset, stopAt)
            break
        }
    }

    wg.Done()
}

func printBusses(busses []*Bus) {
    tmp := ""

    for i := 0; i < len(busses); i++ {
        tmp += fmt.Sprintf("%v", *busses[i])
        if i != len(busses)-1 {
            tmp += " "
        }
    }
    fmt.Println(tmp)
}
