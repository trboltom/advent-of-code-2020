package main

import (
    "fmt"
    "io/ioutil"
    "strconv"
    "strings"
)

const maintenanceId = "x"

type Bus struct {
    id          int
    waitingTime int
}

func loadData(fileName string) (int, []*Bus) {
    rawInput, _ := ioutil.ReadFile(fileName)
    sanitized := strings.TrimRight(string(rawInput), "\n")
    input := strings.Split(sanitized, "\n")

    earliestDeparture, _ := strconv.Atoi(input[0])
    busValues := strings.Split(input[1], ",")

    busses := make([]*Bus, 0)
    for _, singleBusId := range busValues {
        if singleBusId == maintenanceId {
            continue
        }
        id, _ := strconv.Atoi(singleBusId)
        busses = append(busses, &Bus{id: id})
    }

    return earliestDeparture, busses
}

func main() {
    run("demo.txt")
    run("input.txt")
}

func run(fileName string) {
    earliestDeparture, busses := loadData(fileName)

    for _, bus := range busses {
        bus.findClosesDeparture(earliestDeparture)
        fmt.Println(bus)
    }

    bestMatch := findEarliestDeparture(busses)
    fmt.Println("best match is ", bestMatch.id, " with result: ", bestMatch.id*bestMatch.waitingTime)
}

func findEarliestDeparture(busses []*Bus) *Bus {
    bestMatch := busses[0]

    for _, bus := range busses {
        if bus.waitingTime < bestMatch.waitingTime {
            bestMatch = bus
        }
    }
    return bestMatch
}

func (b *Bus) findClosesDeparture(earliestDeparture int) {
    b.waitingTime = b.id - (earliestDeparture % b.id)
}
