package main

import (
    "fmt"
    "io/ioutil"
    "strconv"
    "strings"
)

const TARGET_NUMBER = 2020

func main() {
    rawInput, err := ioutil.ReadFile("./input.txt")
    checkError(err)
    numbers := strings.Split(string(rawInput), "\n")

    for index, first := range numbers {
        for _, second := range numbers[index:] {
            first, err := strconv.Atoi(first)
            checkError(err)
            second, err := strconv.Atoi(second)
            checkError(err)

            if (first + second) == TARGET_NUMBER {
                fmt.Println(first * second)
                return
            }
        }
    }
}

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}
