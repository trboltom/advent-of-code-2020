package main

import (
    "fmt"
    "io/ioutil"
    "regexp"
    "strconv"
    "strings"
)

type Pair struct {
    bagName string
    count   int
}

type Bag struct {
    name        string
    containedIn []Pair
    contains    []Pair
}

func newBag(name string) Bag {
    tmp := Bag{name: name}
    tmp.containedIn = make([]Pair, 0)
    tmp.contains = make([]Pair, 0)
    return tmp
}

var matches = make(map[string]int)

func main() {
    rawInput, _ := ioutil.ReadFile("input.txt")
    lines := strings.Split(string(rawInput), "\n")

    allBags := make(map[string]Bag)
    for _, line := range lines {
        if line == "" {
            continue
        }
        processLine(line, allBags)
    }

    findAllBagsContainingGold(allBags["shiny-gold-bag"], allBags)
    fmt.Println("allBags containing gold: " + strconv.Itoa(len(matches)))
    fmt.Println("gold contains: " + strconv.Itoa(countBagsContainedInGold(allBags["shiny-gold-bag"], allBags)-1))
}

func findAllBagsContainingGold(bag Bag, allBags map[string]Bag) {
    for _, pair := range bag.containedIn {
        matches[pair.bagName]++
        findAllBagsContainingGold(allBags[pair.bagName], allBags)
    }
}

func countBagsContainedInGold(bag Bag, allBags map[string]Bag) int {
    tmp := 1
    for _, pair := range bag.contains {
        tmp += pair.count * countBagsContainedInGold(allBags[pair.bagName], allBags)
    }
    return tmp
}

func processLine(line string, bags map[string]Bag) {
    bagNameRegex := regexp.MustCompile("\\d*\\s?[a-z]+ [a-z]+ bags?")
    allBags := make([]string, 0)
    for _, bag := range bagNameRegex.FindAllString(line, -1) {
        allBags = append(allBags, sanitizeBagName(bag))
    }

    primaryBagName := sanitizeBagName(allBags[0])
    otherBags := allBags[1:]

    primaryBag, ok := bags[primaryBagName]
    if !ok {
        bags[primaryBagName] = newBag(primaryBagName)
        primaryBag = bags[primaryBagName]
    }

    for _, otherBagNameAndCount := range otherBags {
        cntRegex := regexp.MustCompile("^\\d+")
        bagNameRegex := regexp.MustCompile("[a-z][a-z\\-]+")

        count, _ := strconv.Atoi(cntRegex.FindString(otherBagNameAndCount))
        bagName := bagNameRegex.FindString(otherBagNameAndCount)
        bagName = sanitizeBagName(bagName)

        bag, ok := bags[bagName]
        if !ok {
            bags[bagName] = newBag(bagName)
            bag = bags[bagName]
        }

        bag.containedIn = append(bag.containedIn, Pair{primaryBagName, count})
        bags[bagName] = bag

        primaryBag.contains = append(primaryBag.contains, Pair{bagName, count})
        bags[primaryBagName] = primaryBag
    }
}

func sanitizeBagName(name string) string {
    name = strings.Replace(name, "bags", "bag", -1)
    return strings.Replace(name, " ", "-", -1)
}
