package main

import (
    "fmt"
    "io/ioutil"
    "regexp"
    "strconv"
    "strings"
)

func main() {
    input := readInput()
    groups := strings.Split(input, "\n\n")

    anyYes, allYes := countAnswers(groups)
    fmt.Println("Summary of questions for any yes answers: " + strconv.Itoa(anyYes))
    fmt.Println("Summary of questions for all yes answers: " + strconv.Itoa(allYes))
}

func countAnswers(groups []string) (int, int) {
    anyYes := 0
    allYes := 0

    for _, group := range groups {
        uniqueAnswers := make(map[string]int)
        groupSize := strings.Count(group, "\n") + 1
        concatenatedAnswers := strings.Replace(group, "\n", "", -1)

        for i := 0; i < len(concatenatedAnswers); i++ {
            answer := concatenatedAnswers[i : i+1]
            uniqueAnswers[answer]++
        }

        for _, count := range uniqueAnswers {
            if count == groupSize {
                allYes++
            }
        }

        anyYes += len(uniqueAnswers)
    }
    return anyYes, allYes
}

func readInput() string {
    rawInput, _ := ioutil.ReadFile("./input.txt")
    lastEmptyLineRegex := regexp.MustCompile("\n$")
    return lastEmptyLineRegex.ReplaceAllString(string(rawInput), "")
}
