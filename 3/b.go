package main

import (
    "fmt"
    "io/ioutil"
    "strings"
)

func main() {
    slopes := [][]int{{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}}

    rawInput, _ := ioutil.ReadFile("./input.txt")
    worldMap := strings.Split(string(rawInput), "\n")

    sum := 1
    for _, slope := range slopes {
        sum *= calculateSlope(slope[0], slope[1], worldMap)
    }
    fmt.Println(sum)
}

func calculateSlope(stepsToTheRight int, stepsDown int, worldMap []string) int {
    height := len(worldMap) - 1 // empty line at the end of the file
    width := len(worldMap[0])

    currentX := 0
    cnt := 0
    for currentY := stepsDown; currentY < height; currentY += stepsDown {
        currentX = (currentX + stepsToTheRight) % width
        if isTree(worldMap[currentY][currentX : currentX+1]) {
            cnt++
        }
    }
    return cnt
}

func isTree(pointOnMap string) bool {
    return pointOnMap == "#"
}
