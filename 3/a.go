package main

import (
    "fmt"
    "io/ioutil"
    "strings"
)

func main() {
    rawInput, _ := ioutil.ReadFile("./input.txt")
    worldMap := strings.Split(string(rawInput), "\n")

    cnt := getCntForSlope(1, 1, worldMap)
    fmt.Println(cnt)
}

func getCntForSlope(stepsToTheRight int, stepsDown int, worldMap []string) int {
    height := len(worldMap) - 1 // empty line at the end of the file
    length := len(worldMap[0])
    currentX := 0
    cnt := 0
    for currentY := 1; currentY < height; currentY += stepsDown {
        currentX = (currentX + 3) % length
        if isTree(worldMap[currentY][currentX : currentX+1]) {
            cnt++
        }
    }
    return cnt
}

func isTree(pointOnMap string) bool {
    return pointOnMap == "#"
}
